import React from 'react';
import '../App.css';

export default class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            mdp: '',
            data: [],
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);
    }
    handleChange(event) {
        this.setState({ [event.target.name]: event.target.value })
    }
    handleSubmit(event) {
        event.preventDefault();
        this.props.loged(true)
    }
    handleRedirect(){
        this.props.signRedirect("Register")
    }
    render() {
        return (
            <div>
                <div className='bg-grey-100 py-5'>
                    <h1 className='h1 text-center'>Sign in</h1>
                </div>
                <div className='d-flex t-between'>
                    <div></div>
                    <div id='register-components'>
                        <form onSubmit={this.handleSubmit} className="h5 py-2">
                            <p className='text-start bold'><label />Email:</p>
                            <p><input className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="email" value={this.state.email} onChange={this.handleChange} required/></p>
                            <p className='text-start bold'><label />Password:</p>
                            <p><input className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="mdp" value={this.state.mdp} onChange={this.handleChange} required/></p>
                            <p className='text-center'><input className='mt-3 bg-black c-white h4 px-6 py-1 radius-1 py' type="submit" value="Sign Up" /></p>
                            <p className='text-center'>Don't have an account yet?<br></br><span onClick={this.handleRedirect} className='t-underline'>Create an account</span></p>
                        </form>
                    </div>
                    <div> </div>
                </div>
            </div>
        )
    }
}