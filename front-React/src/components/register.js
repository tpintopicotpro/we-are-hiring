import React from 'react';

export default class Register extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            mdp: '',
            comfirmMdp: '',
            err: '',
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchPostFunction = this.fetchPostFunction.bind(this);
        this.handleRedirect = this.handleRedirect.bind(this);
    }

    handleChange(event) {         
        this.setState({[event.target.name]: event.target.value})
    }
    handleSubmit(event) {
        event.preventDefault();
        this.fetchPostFunction();
    }
    handleRedirect(){
        this.props.signRedirect("Login");
    }
    fetchPostFunction() {
        if(this.state.comfirmMdp === this.state.mdp){
        fetch('http://localhost:8000/api/register', {
            method: 'POST',
            // mode: 'no-cors',
            // body: JSON.stringify(this.state.email, this.state.mdp),
            body: (new URLSearchParams({
                email: this.state.email,
                mdp: this.state.mdp,
                comfirmMdp: this.state.comfirmMdp

            })
            )
        }
        )
    }
    else{
        this.setState({err: 'wrong password !'})
    }
    }
    render() {
        return (
            <div>
                <div className='bg-grey-100 py-5'>
                    <h1 className='h1 text-center'>Sign up</h1>
                </div>
                <div className='d-flex t-between'>
                    <div></div>
                    <div id='register-components'>
                        <form onSubmit={this.handleSubmit} className="h5 py-2">
                            <p className='text-start bold'><label />Email:</p>
                            <p><input className='wid-77vw-page h3 px-1 py-1 radius-1' required type="text" value={this.state.email} name="email" onChange={this.handleChange} /></p>
                            
                            <p className='text-start bold'><label />Password:</p>
                            <p><input className='wid-77vw-page h3 px-1 py-1 radius-1' required type="text" value={this.state.mdp} name="mdp" onChange={this.handleChange} /></p>
                            {this.state.err}
                            <p><label />Confirm Password:</p>
                            <p><input className='wid-77vw-page h3 px-1 py-1 radius-1' required type="text" value={this.state.comfirmMdp} name="comfirmMdp" onChange={this.handleChange} /></p>
                            <p className='text-center'><input className='mt-3 bg-black c-white h4 px-6 py-1 radius-1 py' type="submit" value="Sign Up" /></p>
                            <p className='text-center'>You already have an account yet?<br></br><span onClick={this.handleRedirect}className='t-underline'>Sign in</span></p>
                        </form>
                    </div>
                    <div> </div>
                </div>
            </div>
        );
    }
}
