import '../App.css';
import React from 'react';
import Thanks from './thank-you-candidature';

export default class FormCandidature extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            title: '',
            page: 'form-candidature',
            email: '',
        }
      
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleInputChange(event) {
        this.setState({ value: event.target.value });
    }

    

    componentDidMount() {
        
            const apiUrl = `http://localhost:8000/offre/1/`;
            fetch(apiUrl)
                .then(response => {
                    response.json()
                        .then(json => {
                            this.setState({ title: json });
                        })
                })
            fetch('http://localhost:8000/userz')
            .then(response => {
                response.json()
                    .then(user => {
                        this.setState({ user: user });
                    
                    })
            })
    
    }
    
    fetchPost() {
    
        fetch('http://localhost:8000/useroffre', {
            method: 'POST',
            body: (new URLSearchParams({
                email: this.state.email,
                offre_id: this.state.id,
            })
            )
        }
        )
    }

    handleChange(event) {         
        this.setState({[event.target.name]: event.target.value})
        this.setState({id: this.props.data.id})
    }

    handleSubmit(event) {
        event.preventDefault();
        this.fetchPost();
        this.setState({page: 'thanks'})
    }

    render() {
        if (this.state.page === 'form-candidature') {
            return (

                <div>
                    <div className='bg-grey-100 py-5'>
                        <h1 className='h1 text-center'>Apply</h1>
                    </div>



                    <div className='d-flex t-between'>
                        <div></div>
                        <div>
                            <div className='bold'>
                                <p className='h3'>Apply to </p>
                                <p className='h2'>{this.props.data.title}</p>
                            </div>
                            <form id='components-form-candidature' className='h5 py-2' onSubmit={this.handleSubmit}>

                                <p className='text-start bold'><label />Your firstname <span className='c-red'>*</span></p>
                                <p><input placeholder='Edy' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="first-name" onChange={this.handleChange} required /></p>

                                <p className='text-start bold'><label />Your lastname <span className='c-red'>*</span></p>
                                <p><input placeholder='Murphy' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="last-name" onChange={this.handleChange} required /></p>

                                <p className='text-start bold'>Your email address <span className='c-red'>*</span></p>
                                <p><input placeholder='edy.murphy@gmail.com' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="email" value={this.state.email} onChange={this.handleChange} required /></p>

                                <p className='text-start bold'><label />Your phone Number address</p>
                                <p><input placeholder='+33 6 13 78 09 78' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="tel" onChange={this.handleChange} /></p>

                                <p className='text-start bold'><label />Your address</p>
                                <p><input placeholder='775 alabama street' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="adresse" onChange={this.handleChange} /></p>
                                <p><input placeholder='Block C' className='mt-1 wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="adresse" onChange={this.handleChange} /></p>
                                <div className='d-flex t-between'>
                                    <p><input placeholder='31000' className='radius-1 py-1 mt-1 h3 px-1vw wid-20vw-page' type="text" name="adresse" onChange={this.handleChange} /></p>
                                    <span className=''></span>
                                    <p><input placeholder='Toulouse' className='radius-1 py-1 mt-1 h3 px-1vw wid-53vw-page' type="text" name="adresse" onChange={this.handleChange} /></p>
                                </div>
                                <p className='text-start bold'><label />Your linkedIn profil </p>
                                <p><input placeholder='https://...' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="linkedin" onChange={this.handleChange} /></p>
                                <p className='text-start bold'><label />Your GitHub / Gitlab profil </p>
                                <p><input placeholder='https://...' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="gitlab" onChange={this.handleChange} /></p>
                                <p className='text-center'><input className='mt-3 bg-black c-white h4 px-6 py-1 radius-1 py' type="submit" value="Apply" /></p>
                            </form>
                        </div>
                        <div></div>
                    </div>

                </div>
            );
        
        }
        else if (this.state.page === 'thanks') {
            return (
                <Thanks title={this.props.data.title} />
            )
        }
    }
}
/* <div className='text-center'>
                    <div className='bg-grey-100 py-5'>
                        <h1 className='h1 text-center'>Apply</h1>
                    </div>

                    <div className='text-start px-10vw bold'>
                        <p className='h3'>Apply to </p>
                        <p className='h2'>{this.state.title.title}</p>
                    </div>

                    <form id='components-form-candidature' className='px-10vw h5 py-2' onSubmit={this.handleSubmit}>

                        <p className='text-start bold'><label />First name: <span className='c-red'>*</span></p>
                        <p><input placeholder='Edy' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="first-name" onChange={this.handleChange} required /></p>

                        <p className='text-start bold'><label />Last name: <span className='c-red'>*</span></p>
                        <p><input placeholder='Murphy' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="last-name" onChange={this.handleChange} required /></p>

                        <p className='text-start bold'><label />Email: <span className='c-red'>*</span></p>
                        <p><input placeholder='edy.murphy@gmail.com' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="email" onChange={this.handleChange} required /></p>

                        <p className='text-start bold'><label />Téléphone:</p>
                        <p><input placeholder='+33 6 13 78 09 78' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="tel" onChange={this.handleChange} /></p>

                        <p className='text-start bold'><label />Adresse:</p>
                        <p><input placeholder='775 alabama street' className='wid-77vw-page h3 px-1 py-1 radius-1' type="text" name="adresse" onChange={this.handleChange} /></p>
                        <div className='d-flex mt-1'>
                            <input placeholder='775 alabama street' className='wid-20vw-page h3 px-1 py-1 radius-1' type="text" name="adresse" onChange={this.handleChange} />
                            <input placeholder='775 alabama street' className='wid-54vw-page h3 px-1 py-1 radius-1' type="text" name="adresse" onChange={this.handleChange} />
                        </div>
                        <p><input className='mt-3 bg-black c-white h4 px-6 py-1 radius-1 py' type="submit" value="validé" /></p>
                    </form>

                </div> */