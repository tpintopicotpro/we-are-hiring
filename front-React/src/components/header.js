import '../App.css';
import React from 'react';
export default class Header extends React.Component{
    constructor(props) {
        super(props)
    }
    render(){
    return (
    <div className='bg-grey-100 py-5'>
            <div className='text-center py-5'>
            <h1 className='h2'>Finding a job has never <br></br> been easier</h1>
            <p className='my-2'>We hiring allows you to find the best job offers on <br></br> the market and apply quickly and securely</p>
            
            <button className='px-6 py-105 bg-black radius-1 c-white h5' onClick={() =>this.props.handleClick("Offers")}>
                <a>See all offers</a>
            </button>
            </div>
        </div>
        
    )

    }
    
}