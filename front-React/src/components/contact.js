import { isContentEditable } from '@testing-library/user-event/dist/utils';
import React from 'react';
import '../App.css';
import Cardoffer from './read';

export default function Contact(props) {
    return (
        <div>
            <div className='bg-grey-100 py-5'>
                <main className='py-5'>
                    <h1 className='text-center h1'>Offers</h1>
                    <div id='offers'>
                        <section className='d-flex t-between text-center mx-5vw py-5'>
                            {props.data.map(elm => (

                                <article className='wid-30vw-page bg-white py-2 px-1 radius'>
                                    <div className='px-5vw'>
                                        <div className='d-flex t-between'>
                                            <div className='my-auto'>
                                                <p className='text-start'>23.02.2022</p>
                                            </div>
                                            <div>
                                                <section className='d-flex z-20'>
                                                    <div className='starbox'>
                                                        <div id='starbox-id'>
                                                            <div className='star'>
                                                                <div className='big'></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className='starbox z-90 p-absolute-transform'>
                                                        <div id='starbox-new'>
                                                            <div className='star'>
                                                                <div className='small'></div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </section>
                                            </div>

                                        </div>
                                        <h1>{elm.title}</h1>,
                                        <p>{elm.content}</p>

                                    </div>
                                    <div className='d-flex t-between mt-3'>
                                        <p>Toulouse-CDI</p>
                                        <button className='btn-style-none btn-style-go'><a id={elm.id} onClick={<Cardoffer data={elm} />}>See More </a></button>
                                    </div>
                                </article>


                            )
                            )

                            }
                        </section>
                    </div>
                </main>
            </div>
        </div>
    )
}