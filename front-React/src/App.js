import logo from './logo.svg';
import './App.css';
import Navbar from './components/navbar'
import Footer from './components/footer'
import About from './components/about';
import FormContact from './pages/Contact/contact-us'
import Accueil from './pages/Accueil/Accueil'
import LastOffer from './pages/Accueil/last-offer'
import React from 'react';
import ReadCard from './components/read';
import Register from './components/register';
import Login from './components/login';
import Candidature from './pages/Candidature/candidature'
import Favoris from './components/favoris';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      onligne: false,
      page: "Accueil",
      data : {}
    }

    this.handleClick = this.handleClick.bind(this)
    this.getData = this.getData.bind(this)
    this.connecter = this.connecter.bind(this)
  } 
  getData(elm){ 
    this.setState({data : elm})
  }
  handleClick(string) {
    this.setState({ page: string })
  }
  connecter(bolean){
    this.setState({ onligne: bolean })
  }
  render() {
    return (
      <div>
        <Navbar callback={this.handleClick} isLoged={this.state.onligne} logOut={this.connecter}/>
        {this.state.page === "Accueil" && <Accueil getData={this.getData} handleClick={this.handleClick} page={this.state.page}/>}
        {this.state.page === "Offers" &&<LastOffer handleClick={this.handleClick} getData={this.getData} page={this.state.page} onligne={this.state.onligne}/>}
        {this.state.page === "Login" && <Login loged={this.connecter} signRedirect={this.handleClick}/>}
        {this.state.page === "Favoris" && <Favoris />}
        {this.state.page === "Contact" && <FormContact />}
        {this.state.page === "Read" && <ReadCard page={this.state.page} data={this.state.data} offerRedirect={this.handleClick} isLoged={this.state.onligne}/>}
        {this.state.page === 'Register' && <Register signRedirect={this.handleClick}/>}
        {this.state.page === 'Candidature' && <Candidature />}
        
        <Footer />
      </div>
    )
    
}
}
export default App;
